use tracing::info;

use crate::model::{project::Project, user::User};

pub fn users_init() -> Vec<User> {
    vec![
        User::new(1, "admin".to_string(), "admin".to_string()),
        User::new(2, "user".to_string(), "user".to_string()),
    ]
}

pub fn projects_init() -> Vec<Project> {
    vec![
        Project::new(1, "project1".to_string(), "project1".to_string()),
        Project::new(2, "project2".to_string(), "project2".to_string()),
    ]
}
