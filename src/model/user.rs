use serde::{Deserialize, Serialize};
use tracing::info;

#[derive(Debug, Clone, Deserialize, Serialize)]

pub struct User {
    pub id: i64,
    pub username: String,
    pub password: String,
}

impl User {
    pub fn new(id: i64, username: String, password: String) -> Self {
        info!("new user");
        User {
            id,
            username,
            password,
        }
    }
}
