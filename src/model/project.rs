use serde::{Deserialize, Serialize};
use tracing::info;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Project {
    pub id: i64,
    pub name: String,
    pub description: String,
}

impl Project {
    pub fn new(id: i64, name: String, description: String) -> Self {
        info!("Project::new");

        Project {
            id,
            name,
            description,
        }
    }
}
