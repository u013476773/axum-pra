use crate::{dto::user::UserRegisterRequest, model::user::User};

// 查找已经存在user的数量
pub fn create_user_id(users: Vec<User>) -> i64 {
    users.len() as i64 + 1
}

pub fn user_register(users: Vec<User>, req: UserRegisterRequest) -> User {
    let user = User {
        id: create_user_id(users.clone()),
        username: req.username,
        password: req.password,
    };

    user
}

pub fn user_list(users: Vec<User>) -> Vec<User> {
    users
}
