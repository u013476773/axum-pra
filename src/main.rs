use axum::{
    routing::{get, post},
    Router,
};
use axum_pra::handler;
use axum_pra::state::app_state_init;
use axum_pra::util::log;
use std::sync::{Arc, Mutex};
#[tokio::main]
async fn main() {
    // 初始化state
    let state = app_state_init();
    let shared_state = Arc::new(Mutex::new(state));
    // 初始化日志
    log::log_init();

    let addr = "127.0.0.1:3000";

    let listener = tokio::net::TcpListener::bind(addr)
        .await
        .expect("Failed to bind");

    let app = axum::Router::new()
        .route(
            "/user",
            get(handler::user::user_list).post(handler::user::user_register),
        )
        .with_state(shared_state);

    axum::serve(listener, app).await.unwrap();
}
