use serde::Deserialize;
use serde::Serialize;

use crate::constant::projects_init;
use crate::constant::users_init;
use crate::model::project::Project;
use crate::model::user::User;
use std::sync::{Arc, Mutex};

#[derive(Clone, Deserialize, Serialize)]
pub struct AppState {
    pub users: Vec<User>,
    pub projects: Vec<Project>,
}

pub fn app_state_init() -> AppState {
    AppState {
        users: users_init(),
        projects: projects_init(),
    }
}
pub struct AppState2 {
    pub users: Arc<Mutex<Vec<User>>>,
    pub projects: Arc<Mutex<Vec<Project>>>,
}
