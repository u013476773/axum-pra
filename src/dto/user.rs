use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct UserRegisterRequest {
    pub username: String,
    pub password: String,
}
