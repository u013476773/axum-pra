use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ProjectRegisterRequest {
    pub name: String,
    pub description: String,
}
