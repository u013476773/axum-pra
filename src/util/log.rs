use axum::routing::trace;
use tracing_subscriber::{fmt, FmtSubscriber};

pub fn log_init() {
    let log_file = std::fs::File::create("log.log").expect("log.log create failed");

    let subscriber1 = FmtSubscriber::builder()
        .with_max_level(tracing::Level::INFO)
        .finish();

    let subscriber = fmt()
        .pretty()
        .with_file(true)
        .with_writer(log_file)
        .with_ansi(false)
        .with_max_level(tracing::Level::TRACE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
}
