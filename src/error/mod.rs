use std::sync::PoisonError;

use axum::response::IntoResponse;

pub enum AppError {
    PoisonError,
}

// Mutex Error To AppError
impl<T> From<PoisonError<T>> for AppError {
    fn from(p: PoisonError<T>) -> Self {
        AppError::PoisonError
    }
}

// AppError To Response
impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        match self {
            AppError::PoisonError => (
                axum::http::StatusCode::INTERNAL_SERVER_ERROR,
                "PoisonError".to_string(),
            )
                .into_response(),
        }
    }
}
